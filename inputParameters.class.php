<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 19. 2. 2015
 */

/**
 * Třída zpracovává vstupní parametry skriptu
 */
class InputParameters 
{
    
    private static $typeOfAction = NORMALIZATION;  // typ akce, kterou skript provede, např. vypsání nápovědy, determinizace KA, ...
    private static $inputFSM;                      // řetězec, který představuje vstupní KA, jež bude dál zpracován
    private static $typeOfInput = STDOUTT;         // typ vstupu skriptu: standardní vstup vs výstup ze souboru
    private static $inputFile;                     // jméno vstupního souboru (pokud jde vstup ze souboru)
    private static $typeOfOutput = STDOUTT;        // typ výstupu skriptu: standardní výstup vs výstup do souboru
    private static $outputFile;                    // jméno výstupního souboru (pokud jde výstup do souboru)
    private static $FSMCaseInsensitive = false;    // určuje, jestli se bude vstupní automat převádět na malá písmena
    private static $stringToAnalyze = "";          // řetezec, který se bude analyzovat, jestli jej přijme KA
    
    /**
     * Metoda ověří formát vstupních parametrů a formální požadavky na ně kladené
     * @param $argv Pole argumentů předaných skriptu
     * @return true nebo false v závislosti na formální správnost parametrů
     */
    public static function checkTheFormatOfParams($argv) 
    {
        // proměnné, které určují kolikrát se daný parametr vyskytl v předaných argumentech
        $numberOfParamCaseInsensitive = 0;
        $numberOfParamDeterminization = 0;
        $numberOfParamAnalyzeString = 0;
        $numberOfParamNoEpsRules = 0;
        $numberOfParamHelp = 0;
        $numberOfParamInput = 0;
        $numberOfParamOutput = 0;
        // příznak, že se již nějaký parametr vyskytl (hlídáme tak možnost zadání --help jako jediného parametru)
        $someParamOccured = false;
        
        $argv = array_slice($argv, 1); // odstraníme první parametr (název skriptu)
        foreach ($argv as $param) 
        {
            // pokud se jedná o parametr case-insensitive, který se zatím nevyskytl a nevyskytl se ani parametr help
            if (($param == "--case-insensitive" || $param == "-i")
                && $numberOfParamCaseInsensitive == 0 && $numberOfParamHelp == 0) 
            { 
                self::$FSMCaseInsensitive = true;
                $numberOfParamCaseInsensitive++;
            }
            // pokud se jedná o parametr determinization, který se zatím nevyskytl 
            // a nevyskytl se ani parametr no-epsilon-rules ani parametr analyze-string a nevyskytl se ani parametr help
            else if (($param == "--determinization" || $param == "-d")
                     && $numberOfParamAnalyzeString == 0 && $numberOfParamDeterminization == 0 
                     && $numberOfParamNoEpsRules == 0 && $numberOfParamHelp == 0) 
            { 
                self::$typeOfAction = DETERMINIZATION;
                $numberOfParamDeterminization++;
            }
            // pokud se jedná o parametr no-epsilon-rules, který se zatím nevyskytl 
            // a nevyskytl se ani parametr determinization ani parametr analyze-string a nevyskytl se ani parametr help
            else if (($param == "--no-epsilon-rules" || $param == "-e") 
                     && $numberOfParamAnalyzeString == 0 && $numberOfParamNoEpsRules == 0 
                     && $numberOfParamDeterminization == 0 && $numberOfParamHelp == 0) 
            { 
                self::$typeOfAction = REMOVE_EPSILON_TRANSITIONS;
                $numberOfParamNoEpsRules++;
            }
            // pokud se jedná o parametr analyze-string, který se zatím nevyskytl 
            // a nevyskytl se ani parametr determinization ani parametr no-epsilon-rules a nevyskytl se ani parametr help
            else if ((strpos($param, "--analyze-string=") !== false) 
                     && $numberOfParamAnalyzeString == 0 && $numberOfParamNoEpsRules == 0 
                     && $numberOfParamDeterminization == 0 && $numberOfParamHelp == 0) 
            { 
                // načteme hodnotu parametru
                $paramVal = explode("=", $param);
                $paramVal = $paramVal[1];
                
                self::$typeOfAction = ANALYZE_STRING;
                self::$stringToAnalyze = $paramVal;
                $numberOfParamAnalyzeString++;
            }
            // pokud se jedná o parametr help, který je prvním parametrem
            else if ($param == "--help" && $numberOfParamHelp == 0 && $someParamOccured == false) 
            { 
                self::$typeOfAction = PRINT_HELP;
                $numberOfParamHelp++;
            }
            // pokud se jedná o parametr input, který se zatím nevyskytl a který obsahuje hodnotu 
            // a nevyskytl se ani parametr help
            else if (strpos($param, "--input=") !== false 
                     && $numberOfParamInput == 0 && $numberOfParamHelp == 0) 
            {     
                // načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                $paramVal = explode("=", $param);
                $paramVal = $paramVal[1];
                if ($paramVal == "")
                {
                    return false;
                }

                self::$typeOfInput = FILE;
                self::$inputFile = $paramVal;
                $numberOfParamInput++;
            }
            // pokud se jedná o parametr output, který se zatím nevyskytl a který obsahuje hodnotu
            // a nevyskytl se ani parametr help
            else if (strpos($param, "--output=") !== false
                     && $numberOfParamOutput == 0 && $numberOfParamHelp == 0)
            { 
                
                // načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                $paramVal = explode("=", $param);
                $paramVal = $paramVal[1];
                if ($paramVal == "")
                {
                    return false;
                }

                self::$typeOfOutput = FILE;
                self::$outputFile = $paramVal;
                $numberOfParamOutput++;
            }
            // pokud není formát parametrů správný
            else
            {
                return false;
            }
            
            $someParamOccured = true;
        }
        return true;
    }
    
    /**
     * Metoda vrací hodnotu atributu $typeOfAction
     * @return self::$typeOfAction
     */
    public static function getTypeOfAction() 
    {
        return self::$typeOfAction;
    }
    
    /**
     * Metoda vrací hodnotu atributu $inputFile
     * @return self::$inputFile
     */
    public static function getInputFile() 
    {
        return self::$inputFile;
    }
    
    /**
     * Metoda vrací hodnotu atributu $stringToAnalyze
     * @return self::$stringToAnalyze
     */
    public static function getStringToAnalyze() 
    {
        return self::$stringToAnalyze;
    }
 
    /**
     * Metoda vrací výstupní soubor
     * @return Výstupní soubor
     */
    public static function getOutputFile() 
    {
        if (self::$typeOfOutput == STDOUTT)
        {
            return "php://stdout";
        }
        else 
        {
            return self::$outputFile;
        }
    }
    
    /**
     * Metoda vrací popis KA, který byl předán skriptu (prostřednictvím souboru, nebo standardního vstup) jako řetězec
     * @return $FSMAsString Řetězec, který představuje popis vstupního KA
     */
    public static function getInputFSMAsString() 
    {

        // pokud byl KA předán skriptu prostřednitvím standardního vstupu
        if (self::$typeOfInput == STDOUTT)
        {
            $FSMAsString = file_get_contents("php://stdin");
        }
        // pokud byl předán skriptu název soubor s popisem KA
        else if (self::$typeOfInput == FILE)
        {
            // pokud se jedná o existující soubor, ze kterého lze číst
            if (is_readable(self::$inputFile) == true && is_dir(self::$inputFile) == false)
            {
                $FSMAsString = file_get_contents(self::$inputFile);
            }
            // pokud nelze ze souboru číst
            else
            {
                return false;
            }
        }
        
        // pokud se podařilo načíst do řetězce KA a byl předán parametr --case-insensitive, převedeme KA na malá písmena,
        // abychom s ním dále mohli pracovat způsobem, kdy nerozlišujeme velikost písmen
        if ($FSMAsString !== false && self::$FSMCaseInsensitive == true)
        {
            $FSMAsString = mb_convert_case($FSMAsString, MB_CASE_LOWER);
        }
        return $FSMAsString;
    }
}