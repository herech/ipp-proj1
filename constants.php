<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 19. 2. 2015
 */

// konstanty definující akci
define("PRINT_HELP", 0);
define("DETERMINIZATION", 1);
define("REMOVE_EPSILON_TRANSITIONS", 2);
define("NORMALIZATION", 3);
define("ANALYZE_STRING", 4);

// konstanty definující typ vstupu, výstupu
define("STDOUTT", 4);
define("FILE", 5);

// konstanty definující chybové kódy
define("ALL_OK", 0);
define("ERROR_BAD_FORMAT_OF_PARAMS", 1);
define("ERROR_ANALYZE_STRING", 1);
define("ERROR_FILE_CANNOT_BE_OPENED", 2);
define("ERROR_FILE_CANNOT_BE_WRITTEN", 3);
define("ERROR_LEXICAL_OR_SYNTACTIC", 40);
define("ERROR_SEMANTIC", 41);

// konstanty definující tokeny
define("TOKEN_EOF", -1);
define("TOKEN_LEFT_CURLY_BRACKET", -2);
define("TOKEN_RIGHT_CURLY_BRACKET", -3);
define("TOKEN_LEFT_PARENTHESIS", -4);
define("TOKEN_RIGHT_PARENTHESIS", -5);
define("TOKEN_COMMA", -6);
define("TOKEN_FSM_STATE", -7);
define("TOKEN_FSM_INPUT", -8);
define("TOKEN_TRANSITION", -9);

// ostatní konstanty
define("EPSILON", "''");
define("STRING_HAS_BEEN_ACCEPTED", 1);      // použito v rozšíření STR
define("STRING_HAS_NOT_BEEN_ACCEPTED", 0);  // použito v rozšíření STR

// text nápovědy
define("HELP_MESSAGE",
    "*************************** NÁPOVĚDA ***************************" . PHP_EOL . PHP_EOL
    
    ."*** OBECNÉ INFORMACE ***" . PHP_EOL . PHP_EOL
    ."Skript získává na vstupu konečný automat v textové podobě, "           
    ."dle zadaných parametrů nad tímto KA provádí různé činnosti, "           
    ."hlavní činností je determinizace konečného automatu." . PHP_EOL . PHP_EOL
        
    ."*** FORMÁT VSTUPU ***" . PHP_EOL . PHP_EOL
    ."Komentáře do konce řádku začínají znakem #. Bílé znaky jako konec řádku (\\n i\\r), "
    ."#mezera či tabulátor jsou ignorovány (až na později definované případy). " . PHP_EOL . PHP_EOL
    
    ."Stav je reprezentován identifikátorem jazyka C, který nezačíná ani nekončí podtržítkem. "
    ."Vstupní symbol je reprezentovaný libovolným znakem uzavřeným v apostrofech. " 
    ."U stavů i vstupních symbolů záleží na velikosti písmen. " 
    ."Jako vstupní symboly lze využít i metaznaky popisu automatu a všechny bílé znaky, " 
    ."tj. '(', ')', '{', '}', '''', '-', '>', ',', '.', '#', ' ', znak konce řádku v apostrofech " 
    ."a znak tabulátoru v apostrofech. Apostrof musí být navíc uvnitř apostrofů zdvojený. " 
    ."Prázdná dvojice apostrofů '' reprezentuje prázdný řetězec. " . PHP_EOL . PHP_EOL
    
    ."Celý konečný automat je zapisován podobnou notací jako ve formálních jazycích (IFJ). "  
    ."Celý konečný automat je pětice uzavřená do kulatých závorek. "  
    ."Každá komponenta kromě komponenty určující počáteční stav je dále uzavřena ve složených závorkách " 
    ."a od ostatních komponent oddělena čárkou. Jednotlivé prvky množin reprezentujících komponenty " 
    ."(opět kromě počátečního stavu) jsou odděleny také čárkou. " . PHP_EOL . PHP_EOL
    
    ."Nejprve je definována konečná množina stavů, následuje neprázdná vstupní abeceda, " 
    ."poté definice množiny pravidel, dále určení počátečního stavu a nakonec množina koncových stavů. "  
    ."Množina pravidel je popsána seznamem pravidel. " 
    ."Každé pravidlo je zapsáno ve tvaru: pa -> q, kde p je výchozí stav, a je čtený vstupní symbol "
    ."(případně prázdný řetězec), následuje dvojznak pomlčka s většítkem reprezentující šipku " 
    ."(tento dvojznak nesmí být rozdělen jiným znakem) a poslední část pravidla q určuje cílový stav. " . PHP_EOL . PHP_EOL
    
    ."*** PŘÍKLAD VSTUPNÍHO ZÁPISU KA ***" . PHP_EOL . PHP_EOL
    ."# Příklad konečného automatu ve vstupním formátu úlohy DKA" . PHP_EOL
    ."({s, p,q,r ,nonTermState, # stav neukončující i nedostupný" . PHP_EOL
    ."fin1}, # stav bude označen jako koncový" . PHP_EOL
    ."{'a', '''', '{', ')', 'b','č','b' }, {s 'č' -> p," . PHP_EOL
    ."q'a' -> r, q'{'-> r, r 'a' ->r, p'''' ->q ," . PHP_EOL
    ."r ''->s # takto vypadá pravidlo, které nečte vstup; lze i takto: r '' -> s" . PHP_EOL
    ."}," . PHP_EOL
    ."# následuje komponenta definující počáteční stav" . PHP_EOL
    ."r" . PHP_EOL
    .", {fin1, s, r} ) # koncové stavy a ukončení definice automatu" . PHP_EOL
    ."# zde může následovat libovolný počet bílých znaků nebo komentářů" . PHP_EOL . PHP_EOL
        
    ."*** PARAMETRY SKRIPTU ***" . PHP_EOL . PHP_EOL
    ."--help - vypsání nápovědy, nelze kombinovat s jinými parametry" . PHP_EOL . PHP_EOL
    
    ."--input=filename - zadaný vstupní textový soubor v UTF-8 s popisem konečného automatu. " 
    ."Pokud nebude parametr zadán načte se popis konečného automatu ze standardního vstupu." . PHP_EOL . PHP_EOL
    
    ."--output=filename - textový výstupní soubor (opět v UTF-8) s popisem výsledného ekvivalentního " 
    ."konečného automatu v předepsaném formátu. Pokud nebude parametr zadán zapíše se výsledný popis " 
    ."konečného (deterministického) automatu na standardní výstup." . PHP_EOL . PHP_EOL

    ."-e, --no-epsilon-rules - pro pouhé odstranění ε-pravidel vstupního konečného automatu. "
    ."Parametr nelze kombinovat s parametrem -d (resp. --determinization)." . PHP_EOL . PHP_EOL
   
    ."-d, --determinization - provede determinizaci bez generování nedostupných stavů. " 
    ."Parametr nelze kombinovat s parametrem -e (resp. --no-epsilon-rules)." . PHP_EOL . PHP_EOL

    ."-i, --case-insensitive - nebude brán ohled na velikost znaků při porovnávání symbolů či stavů " 
    ."(tj. a = A, ahoj = AhOj nebo Ab = aB); ve výstupu potom budou všechna velká písmena převedena na malá." . PHP_EOL . PHP_EOL 

    ."Pokud nebude uveden parametr -e ani -d, tak dojde pouze k validaci a normalizovanému výpisu načteného konečného automatu." . PHP_EOL 
);
