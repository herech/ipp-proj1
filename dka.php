<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 19. 2. 2015
 * Popis: Skript získává na vstupu konečný automat v textové podobě, 
 *        dle zadaných parametrů nad tímto KA provádí různé činnosti, 
 *        hlavní činností je determinizace konečného automatu.
 */

mb_internal_encoding("UTF-8");
mb_regex_encoding('UTF-8');

include_once "constants.php";
include_once "inputParameters.class.php";
include_once "scanner.class.php";
include_once "parser.class.php";
include_once "FSM.class.php";

$printNormalForm = true; // příznak, jestli se má tisknout normální forma KA

// zkontrolujeme formální správnost parametrů
if (InputParameters::checkTheFormatOfParams($argv) == false) 
{
    file_put_contents("php://stderr", "ERROR_BAD_FORMAT_OF_PARAMS\n");
    exit(ERROR_BAD_FORMAT_OF_PARAMS);
}

// pokud si uživatel přeje vypsat nápovědu, tak ji vypíšeme a ukončíme skript
if (InputParameters::getTypeOfAction() == PRINT_HELP) 
{
    print(HELP_MESSAGE);
    exit(ALL_OK);
}

// získáme vstupní reprezentaci KA jako řetězec
$inputFSM = InputParameters::getInputFSMAsString();
// pokud se nepodařilo načíst KA
if ($inputFSM === false)
{
    file_put_contents("php://stderr", "ERROR_FILE_CANNOT_BE_OPENED\n");
    exit(ERROR_FILE_CANNOT_BE_OPENED);
}

// předáme lexikálnímu analyzátoru řetězcovou reprezentaci vstupního KA
Scanner::setInputFSM($inputFSM);

// započneme analýzu vstupního KA
if (($errorCode = Parser::BeginSyntacticAnalysis()) != ALL_OK)
{
    file_put_contents("php://stderr", "ERROR_PARSER: $errorCode\n");
    exit($errorCode);
}

// provedeme akci, kterou uživatel specifikoval
if (InputParameters::getTypeOfAction() == REMOVE_EPSILON_TRANSITIONS) 
{
    FSM::removeEpsilonTransitions();
}
else if (InputParameters::getTypeOfAction() == DETERMINIZATION)
{
    FSM::makeTheDeterminization();
}
// rozšíření STR, kdy analyzujeme řetězec, jestli jej KA přijme
else if (InputParameters::getTypeOfAction() == ANALYZE_STRING)
{
    if (FSM::stringAnalyze(InputParameters::getStringToAnalyze()) == false)
    {
        file_put_contents("php://stderr", "ERROR_ANALYZE_STRING \n");
        exit(ERROR_ANALYZE_STRING);
    }
    else
    {
        $printNormalForm = false;
    }
}

// nakonec KA vytiskneme v Normální formě
if ($printNormalForm && FSM::printNormalForm(InputParameters::getOutputFile()) == false)
{
    file_put_contents("php://stderr", "ERROR_FILE_CANNOT_BE_WRITTEN \n");
    exit(ERROR_FILE_CANNOT_BE_WRITTEN);
}

// vše proběhlo v pořádku, ukončíme korektně skript
file_put_contents("php://stderr", "\nALL_OK\n");
exit(ALL_OK);