<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 20. 2. 2015
 */

/**
 * Třída reperezentuje lexikální analyzátor pro analýzu vstupní reprezentaci KA
 */
class Scanner {
    
    // Reprezentace vstupního KA jako pole jednotlivých znaků, které bude lex. analyzátor přijímat
    private static $arrayOfCharsOfInputFSM;
    // Počet znaků, ze kterých se skládá vstupní reprezentace KA
    private static $numberOfCharsInInputFSM;
    // Ukazatel do pole na aktuálně načtený znak z řetězcového popisu vstupního KA
    private static $pointerToInputFSM = 0;
    // typ tokenu, který jsme načetli ze vstupu a který vrátíme syntaktickému analyzátoru
    private static $tokenType;
    // hodnota tokenu, který jsme načetli ze vstupu a který vrátíme syntaktickému analyzátoru
    private static $tokenVal = "";
    // Buffer, do kterého se ukládají tokeny, které jsou načteny v syntaktickém analyzátoru přes epsilon pravidla
    // z bufferu pak přednostně načítá funkce getToken(), čímž se eliminují některé problémy při syntaktické analýze
    private static $tokenBuffer = array();
    // Stav lexikálního analyzátoru
    private static $currState = self::STATE_DEFAULT;
    
    // Stavy lexikálního analyzátoru
    const STATE_DEFAULT = 0;              // výchozí stav, kdy je automat připraven načíst nový lexém
    const STATE_EOF = 1;                  // koncový stav, kdy se načetl EOF
    const STATE_LEFT_PARENTHESIS = 2;     // koncový stav, kdy se načetla levá kulatá závorka 
    const STATE_RIGHT_PARENTHESIS = 3;    // koncový stav, kdy se načetla pravá kulatá závorka 
    const STATE_LEFT_CURLY_BRACKET = 4;   // koncový stav, kdy se načetla levá složená závorka 
    const STATE_RIGHT_CURLY_BRACKET = 5;  // koncový stav, kdy se načetla pravá kulatá závorka 
    const STATE_COMMA = 6;                // koncový stav, kdy se načetla čárka
    const STATE_COMMENT = 7;              // nekoncový stav, ve kterém se zpracovává komentář
    const STATE_STATE1 = 8;               // koncový stav, kdy se načetl stav vstupního KA
    const STATE_STATE2 = 9;               // nekoncový stav, kdy se při zpracování stavu vstupního KA načetl znak _
    const STATE_INPUT1 = 10;              // nekoncový stav, kdy se při zpracování vstupu vstupního KA načetl znak '
    const STATE_INPUT2 = 11;              // koncový stav, kdy se při zpracování vstupu vstupního KA načetly 2 znaky ', '
    const STATE_INPUT3 = 12;              // nekoncový stav, kdy se při zpracování vstupu vstupního KA načetly 3 znaky ', ', '
    const STATE_INPUT4 = 13;              // koncový stav, kdy se při zpracování vstupu vstupního KA načetly 4 znaky ', ', ', '
    const STATE_INPUT5 = 14;              // nekoncový stav, kdy se při zpracování vstupu vstupního KA načetly 2 znaky ', not(')
    const STATE_INPUT6 = 15;              // koncový stav, kdy se při zpracování vstupu vstupního KA načetly znaky ', not('), '
    const STATE_DASH = 16;                // nekoncový stav, do kterého přejdeme pokud se z výchozího stavu načte znak –
    const STATE_TRANSITION = 17;          // koncový stav, kdy se načetl dvojznak přechodu –>
    
    // pole kde indexy představují koncové stavy (KA tak bude při ověřování jestli může přijmout nějaký řetězec) 
    private static $setOfFinalStates = array(self::STATE_EOF => 1, 
                                             self::STATE_LEFT_PARENTHESIS => 1,
                                             self::STATE_RIGHT_PARENTHESIS => 1,
                                             self::STATE_LEFT_CURLY_BRACKET => 1,
                                             self::STATE_RIGHT_CURLY_BRACKET => 1,
                                             self::STATE_COMMA => 1,
                                             self::STATE_STATE1 => 1,
                                             self::STATE_INPUT2 => 1,
                                             self::STATE_INPUT4 => 1,
                                             self::STATE_INPUT6 => 1,
                                             self::STATE_TRANSITION => 1
                                            );
    
    /**
     * Metoda převede vstupní řetězcovou reprezentaci KA na pole jednotlivých znaků, které bude lex. analyzátor přijímat
     * @param $inputFSM Řetězcová reprezentace vstupního KA
     * @return void
     */
    public static function setInputFSM($inputFSM)
    {
        self::$arrayOfCharsOfInputFSM = preg_split('//u', $inputFSM, NULL, PREG_SPLIT_NO_EMPTY);
        // uložíme si počet znaků řetězce reprezentujícího vstupní KA
        self::$numberOfCharsInInputFSM = count(self::$arrayOfCharsOfInputFSM);
    }
    
    /**
     * Metoda vrací následující token, je implementována pomocí KA
     * @param $tokenVal Parametr referenčního typu, do kterého uložíme hodnotu tokenu
     * @return typ tokenu v případě chyby false
     */
    public static function getNextToken(&$tokenVal)
    {
        // pokud je fronta tokenů neprázdná, tak namísto čtení tokenu vrátíme první token z fronty
        if (count(self::$tokenBuffer) > 0)
        {
            $tokenFromBuffer = array_shift(self::$tokenBuffer);
            $tokenVal = $tokenFromBuffer[1];
            return $tokenFromBuffer[0];
        }

        self::$currState = self::STATE_DEFAULT; // provedeme reset stavu KA
        self::$tokenVal = "";                   // provedeme reset hodnoty načteného tokenu
        
        // budeme vykonávat smyčku, dokud z ní násilně nevyskočíme, nebo nepřečteme "znak" EOF
        do
        {
            $currChar = self::readNextChar(); // načteme další znak
            
            // implementace KA, kde na základě současného stavu a vstupu určíme výstup a následující stav
            // diagram KA je uveden v dokumentaci
            switch(self::$currState)
            {
                case self::STATE_DEFAULT:
                    if ($currChar == "{")
                    {
                        self::$tokenType = TOKEN_LEFT_CURLY_BRACKET;
                        self::$currState = self::STATE_LEFT_CURLY_BRACKET;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar == "}")
                    {
                        self::$tokenType = TOKEN_RIGHT_CURLY_BRACKET;
                        self::$currState = self::STATE_RIGHT_CURLY_BRACKET;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar == "(")
                    {
                        self::$tokenType = TOKEN_LEFT_PARENTHESIS;
                        self::$currState = self::STATE_LEFT_PARENTHESIS;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar == ")")
                    {
                        self::$tokenType = TOKEN_RIGHT_PARENTHESIS;
                        self::$currState = self::STATE_RIGHT_PARENTHESIS;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar == ",")
                    {
                        self::$tokenType = TOKEN_COMMA;
                        self::$currState = self::STATE_COMMA;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar == TOKEN_EOF)
                    {
                        self::$tokenType = TOKEN_EOF;
                        self::$currState = self::STATE_EOF;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if (ctype_space($currChar))
                    {
                        self::$currState = self::STATE_DEFAULT;
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == "#")
                    {
                        self::$currState = self::STATE_COMMENT;
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == "'")
                    {
                        self::$currState = self::STATE_INPUT1;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if (ctype_alnum($currChar))
                    {
                        self::$currState = self::STATE_STATE1;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == "-")
                    {
                        self::$currState = self::STATE_DASH;
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else
                    {
                        break 2; // nepodporovaný vstupní symbol, ukončíme automat s chybou
                    }
                
                case self::STATE_COMMENT:
                    if ($currChar == "\r" || $currChar == "\n")
                    {
                        self::$currState = self::STATE_DEFAULT;
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == TOKEN_EOF)
                    {
                        self::$tokenType = TOKEN_EOF;
                        self::$currState = self::STATE_EOF;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else if ($currChar != "\r" && $currChar != "\n" && $currChar != TOKEN_EOF)
                    {
                        self::$currState = self::STATE_COMMENT;
                        break; // pokračujeme čtením dalšího znaku
                    }
                case self::STATE_DASH:
                    if ($currChar == ">")
                    {
                        self::$tokenType = TOKEN_TRANSITION;
                        self::$currState = self::STATE_TRANSITION;
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else
                    {
                        break 2; // nepodporovaný vstupní symbol, ukončíme automat s chybou
                    }
                case self::STATE_STATE1:
                    if (ctype_alnum($currChar))
                    {
                        self::$currState = self::STATE_STATE1;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == "_")
                    {
                        self::$currState = self::STATE_STATE2;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else
                    {
                        self::$tokenType = TOKEN_FSM_STATE;
                        self::$currState = self::STATE_STATE1;
                        self::ungetChar(); // vrátíme načtený znak, protože už není součástí tokenu, který reprezentuje stav KA
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                case self::STATE_STATE2:
                    if (ctype_alnum($currChar))
                    {
                        self::$currState = self::STATE_STATE1;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar == "_")
                    {
                        self::$currState = self::STATE_STATE2;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else
                    {
                        break 2; // nepodporovaný vstupní symbol, ukončíme automat s chybou
                    }
                case self::STATE_INPUT1:
                    if ($currChar == "'")
                    {
                        self::$currState = self::STATE_INPUT2;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else if ($currChar != "'")
                    {
                        self::$currState = self::STATE_INPUT5;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break; // pokračujeme čtením dalšího znaku
                    }
                case self::STATE_INPUT2:
                    if ($currChar == "'")
                    {
                        self::$currState = self::STATE_INPUT3;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu;
                        break; // pokračujeme čtením dalšího znaku
                    }
                    else
                    {
                        self::$tokenType = TOKEN_FSM_INPUT;
                        self::$currState = self::STATE_INPUT2;
                        self::ungetChar(); // vrátíme načtený znak, protože už není součástí tokenu, který reprezentuje vstup KA
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                case self::STATE_INPUT3:
                    if ($currChar == "'")
                    {
                        self::$tokenType = TOKEN_FSM_INPUT;
                        self::$currState = self::STATE_INPUT4;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else
                    {
                        break 2; // nepodporovaný vstupní symbol, ukončíme automat s chybou
                    }
                case self::STATE_INPUT5:
                    if ($currChar == "'")
                    {
                        self::$tokenType = TOKEN_FSM_INPUT;
                        self::$currState = self::STATE_INPUT6;
                        self::$tokenVal .= $currChar; // přidáme znak do řetězce reprezentující hodnotu tokenu
                        break 2; // načetli jsme token a vyskočíme až za konstrukci while
                    }
                    else
                    {
                        break 2; // nepodporovaný vstupní symbol, ukončíme automat s chybou
                    }
            }
            
        } while($currChar != TOKEN_EOF);

        // pokud aktuální stav automatu je stavem koncovým, je vše v pořádku, vrátíme typ tokenu a jeho hodnotu
        if (isset(self::$setOfFinalStates[self::$currState]))
        {
            $tokenVal = self::$tokenVal;
            return self::$tokenType;
        }
        // pokud došlo k lexikální chybě (nacházíme se v nekoncovém stavu) vrátíme false
        else
        {
            return false;
        }
        
    }
    
    /**
     * Metoda vrátí následující znak z řětězcové reprezentace vstupního KA
     * @return Vrací znak z pole $arrayOfCharsOfInputFSM na indexu $pointerToInputFSM, 
     *         který je následně inkrementován. Případně EOF, pokud již byly všechny znaky z pole přečtěny.
     */
    private static function readNextChar()
    {
        // pokud se ukazatel do pole pohybuje v rámci mezí pole
        if (self::$pointerToInputFSM < self::$numberOfCharsInInputFSM)
        {
            return self::$arrayOfCharsOfInputFSM[self::$pointerToInputFSM++];
        }
        // pokud ukazatel ukazuje mimo pole, už nejsou na vstupu žádné další znaky a vracíme EOF
        else
        {
            return TOKEN_EOF;
        }
    }
    
    /**
     * Metoda dekrementuje hodnotu atributu $pointerToInputFSM, čímž simuluje chování funkce známé z jazyka C ungetchar
     * @return void
     */
    private static function ungetChar()
    {
        if (self::$pointerToInputFSM > 0)
        {
            self::$pointerToInputFSM--;
        }
    }
    
    /**
     * Metoda ukládá do bufferu přečtený token, tak aby se mohl přečít při dalším volání funkce getToken znovu
     * @return void
     */
    public static function ungetToken($token)
    {
        array_push(self::$tokenBuffer, $token);
    }
    
}
