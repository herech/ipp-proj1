<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 22. 2. 2015
 */

/**
 * Třída reprezentuje načtený konečný automat, nad kterým budeme provádět různé operace
 */
class FSM {
    // pole, které svými indexy reprezentuje množinu stavů
    private static $setOfStates = array();
    // pole, které svými indexy reprezentuje vstupní abecedu
    private static $inputAlphabet = array();
    // pole, které obsahuje množinu pravidel, kde každý pravidlo má následující strukturu: [stav] => array(array([symbol] => stav), ..., ...)
    private static $setOfRules = array();
    // počáteční stav KA
    private static $initialState;
    // pole, které svými indexy reprezentuje množinu koncových stavů
    private static $setOfFiniteStates = array();
    
    // příznaky, které určují které akce byly již nad KA provedenny
    private static $FSMIsdeterministic = false;
    private static $FSMIsWithoutEpisilonTransitions = false;
    
    /**
     * Metoda vrací množinu vstupních symbolů $inputAlphabet
     * @return self::$inputAlphabet
     */
    public static function getInputAlphabet()
    {
        return self::$inputAlphabet;
    }
    
    /**
     * Metoda přidává symbol do vstupní abecedy $inputAlphabet, pokud již existuje, bude přepsán
     * @param $inputSymbol vstupní symbol
     * @return void
     */
    public static function addInputSymbol($inputSymbol)
    {
        self::$inputAlphabet[$inputSymbol] = $inputSymbol;
    }
    
    /**
     * Metoda vrací množinu stavů $setOfStates
     * @return self::$setOfStates
     */
    public static function getStates()
    {
        return self::$setOfStates;
    }
    
    /**
     * Metoda přidává stav KA, pokud již existuje, bude přepsán
     * @param $state stav KA
     * @return void
     */
    public static function addState($state)
    {
        self::$setOfStates[$state] = $state;
    }
    
    /**
     * Metoda vrací množinu pravidel $setOfRules
     * @return self::$setOfRules
     */
    public static function getRules()
    {
        return self::$setOfRules;
    }
    
    /**
     * Metoda přidává pravidlo KA, pokud již existuje, nebude se nové vkládat
     * @param $originalState původní stav, ze kterého je prováděn přechod
     * @param $symbol vstupní symbol 
     * @param $resultantState výsledný stav, do kterého se automat dostane po přechodu
     * @return void
     */
    public static function addRule($originalState, $symbol, $resultantState)
    {        
        // pokud existuje již stejné pravidlo v množině pravidel, nebudeme toto vkládat
        if (array_key_exists($originalState, self::$setOfRules))
        {
            foreach(self::$setOfRules[$originalState] as $symbolAndResultantState)
            {
                if (array_key_exists($symbol, $symbolAndResultantState))
                {
                    if ($symbolAndResultantState[$symbol] == $resultantState)
                    {
                        return;
                    }
                }
            }
        }
        
        self::$setOfRules[$originalState][] = array($symbol => $resultantState);
    }
    
    /**
     * Metoda vrací množinu pravidel $setOfFiniteStates
     * @return self::$setOfFiniteStates
     */
    public static function getFiniteStates()
    {
        return self::$setOfFiniteStates;
    }
    
    /**
     * Metoda přidává koncový stav KA, pokud již existuje, bude přepsáno
     * @param $finiteState koncový stav KA
     * @return void
     */
    public static function addFiniteState($finiteState)
    {
        self::$setOfFiniteStates[$finiteState] = $finiteState;
    }
    
    /**
     * Metoda vrací počáteční stav KA $initialState
     * @return self::$initialState
     */
    public static function getInitialState()
    {
        return self::$initialState;
    }
    
    /**
     * Metoda nastavuje poč. stav KA
     * @param $initialState poč. stav KA
     * @return void
     */
    public static function setInitialState($initialState)
    {
        self::$initialState = $initialState;
    }
    
    /**
     * Metoda vrací pro daný stav jeho epsilon uzávěr
     * @param $state stav pro nejž budeme počítat epsilon uzávěr
     * @return Epsilon uzávěr
     */
    private static function epsilonClosure($state)
    {
        $oldEpsilonClosure = array(); // epsilon uzávěr z minulé iterace algoritmu    
        $newEpsilonClosure = array(); // epsilon uzávěr vypočítaný v současné iteraci algoritmu 
        $i = 0; // pomocné počítadlo
        
        do
        {
            $tmpArray = array(); // pole obsahující nově vypočítané epsilon uzávěry ze současné iterace algoritmu
            // na začátku další iterace nastavíme $oldEpsilonClosure na $newEpsilonClosure, pokud se jedná o první iteraci, 
            // tak provedeme inicializaci 
            $oldEpsilonClosure = ($i > 0) ? $newEpsilonClosure : array($state);
            
            // počítáme nový epsilon uzávěr
            // procházíme množinu stavů
            foreach(self::$setOfStates as $p)
            {
                // procházíme stavy z epsilon uzávěru z minulé iterace
                foreach($oldEpsilonClosure as $q)
                {
                    // hledáme pro stav z epsilon uzávěru epsilon přechod do jiného stavu, ten pak přidáme opět do epislon uzávěru
                    if (array_key_exists($q, self::$setOfRules))
                    {
                        foreach(self::$setOfRules[$q] as $symbolAndResultantState)
                        {
                            if (array_key_exists(EPSILON, $symbolAndResultantState))
                            {
                                // pokud se jedná o epsilon pravidlo, 
                                // přidáme z něj výsledný stav do aktuálně vypočítávaného epsilon uzávěru
                                if ($symbolAndResultantState[EPSILON] == $p)
                                {
                                    $tmpArray[] = $p;
                                }
                            }
                        }
                    }
                }
            }
            
            // nově vypočítaný epsilon uzávěr sloučíme s epsilon uzávěrem vypočítaným v minulé iteraci a dostaneme epsilon uzávěr,
            // který se zase o něco více přibližuje reálnému uzávěru, který hledáme
            $newEpsilonClosure = array_unique(array_merge($oldEpsilonClosure, $tmpArray));
            $i++;
            
        } while($oldEpsilonClosure != $newEpsilonClosure); // dokud je stále možné přidávat stavy do epislon uzávěru
        
        return $newEpsilonClosure;
    }
    
    /**
     * Metoda odstraní epsilon přechody
     * @return void
     */
    public static function removeEpsilonTransitions()
    {
        // pokud již byly odstraněny epsilon přechody, tak už tuto operaci znova provádět nebudeme
        if (self::$FSMIsWithoutEpisilonTransitions)
        {
            return;
        }

        $newSetOfRules = array(); // nová množina pravidel, která vznikne po odstranění epsilon přechodů
        
        // odstraníme epsilon přechody, tzn. upravíme množinu pravidel
        // procházíme množinu stavů
        foreach(self::$setOfStates as $p)
        {
            $epsilonClosure = self::epsilonClosure($p); // epsilon uzávěr aktuálně procházeného stavu
            
            // procházíme stavy z epsilon uzávěru aktuálně procházeného stavu
            foreach($epsilonClosure as $stateFromEpsilonClosure)
            {
                // hledáme v pravidlech přechod, začínající z procházeného stavu z epsilon uzávěru a který neobsahuje jako symbol epsilon,
                // v pravidle splňující dané kritérium nahradíme první stav (z epsilon uzávěru) stavem, který procházíme v nejvyšší itreraci
                // a takto upravené pravidlo přidáme do nové množiny pravidel
                if (array_key_exists($stateFromEpsilonClosure, self::$setOfRules))
                {
                    foreach(self::$setOfRules[$stateFromEpsilonClosure] as $symbolAndResultantState)
                    {
                        foreach($symbolAndResultantState as $symbol => $resultantState)
                        {
                            // do nové množiny pravidel přidáme jen pravidla, které mají vstupní symbol, jež leží ve vstupní abecedě,
                            // to jsou všechna která neobsahují epislon
                            if ($symbol != EPSILON)
                            {
                                self::addRuleToArray($p, $symbol, $resultantState, $newSetOfRules);
                            }
                        }
                    }
                }
            }
        }        
        
        $newSetOfFiniteStates = array(); // nová množina koncových stavů, která vznikne po odstranění epsilon přechodů

        // vytvoříme množinu koncových stavů, které vznikly po odstranění epsilon přechodů
        foreach(self::$setOfStates as $p)
        {
            $epsilonClosure = self::epsilonClosure($p);
            
            $tmpIntersect = array_intersect($epsilonClosure, self::$setOfFiniteStates);
            if (count($tmpIntersect) > 0)
            {
                $newSetOfFiniteStates[$p] = $p;
            }
            
        }
        
        // nahradíme stávající množinu pravidel novou množinou pravidel, která vznikla po odstraněnní epsilon přecodů
        self::$setOfRules = $newSetOfRules; 
        
        // nahradíme stávající množinu koncových stavů novou množinou koncových stavů, které vznikly po odstraněnní epsilon přechodů
        self::$setOfFiniteStates = $newSetOfFiniteStates;
        
        self::$FSMIsWithoutEpisilonTransitions = true; // nastavíme příznak odstraněněí epsilon přechodů

    }
    
    /**
     * Metoda provede determinizaci KA
     * @return void
     */
    public static function makeTheDeterminization()
    {
        // pokud již byla provedena determinizace, nebudeme ji znovu provádět
        if (self::$FSMIsdeterministic)
        {
            return;
        }
        
        self::removeEpsilonTransitions(); // nejprve odstraníme epsilon přechody, pokud ještě odstraněněy nebyly

        $Qnew = array(self::$initialState => self::$initialState);
        $Rd = array(); // nová množina pravidel po determinizaci
        $Qd = array(); // nová množina stavů po determinizaci
        $Fd = array(); // nová množina koncových stavů po determinizaci
        
        $mergedStatesArray = array(); // pole, které obahuje jako klíče názvy sloučených stavů a jako hodnoty obsahuje pole s těmito jednotlivými stavy
        
        do
        {
            $Q1 = array_pop($Qnew);
            $Qd[$Q1] = $Q1; // přidáme stav do množiny nových stavů vzniklých po determinizaci
            
            // pro každý symbol ze vstupní abecedy
            foreach(self::$inputAlphabet as $a)
            {
                // vytváříme sloučený stav z více stavů
                $Q2 = ""; // sloučený stav z více stavů
                $individualStatesFromQ1 = (isset($mergedStatesArray[$Q1])) ? $mergedStatesArray[$Q1] : array($Q1);
                foreach($individualStatesFromQ1 as $stateFromQ1)
                {
                    // pokud to bude možné naplníme proměnnou Q2 stavem q z pravidla p a -> q, kde p = $Q1, a = $a
                    if (array_key_exists($stateFromQ1, self::$setOfRules))
                    {
                        foreach(self::$setOfRules[$stateFromQ1] as $symbolAndResultantState)
                        {
                            foreach($symbolAndResultantState as $symbol => $resultantState)
                            {
                                if ($symbol == $a)
                                {
                                    $Q2 .= $resultantState . "|" ; // $Q2 bude stav sloučený z více stavů                                
                                }
                            }
                        }
                    }
                }
                
                // pokud byl vytvořen nový stav který slučuje více stavů
                if($Q2 != "")
                {
                    $Q2 = substr($Q2, 0, -1); // odstraníme poslední svislítko
                    $Q2Array = explode("|", $Q2); // z řetezce stavů vytvoříme pole
                    $Q2Array = array_unique($Q2Array);
                    self::lexicographicSort($Q2Array); // seřadíme pole lexikograficky
                    $Q2 = implode("_", $Q2Array); // po seřazení stavů se slijeme do jednoho řetězce kde budou mezi sebou odděleny podtržítkem
                    $mergedStatesArray[$Q2] = $Q2Array;
                    self::addRuleToArray($Q1, $a, $Q2, $Rd);
                }
                if(!isset($Qd[$Q2]) && $Q2 != "")
                {
                    $Qnew[$Q2] = $Q2;
                }
                    
            }
            
            $individualStatesFromQ1 = (isset($mergedStatesArray[$Q1])) ? $mergedStatesArray[$Q1] : array($Q1);
            $tmpIntersect = array_intersect($individualStatesFromQ1, self::$setOfFiniteStates);
            if (!empty($tmpIntersect))
            {
                $Fd[$Q1] = $Q1; // přidáme koncový stav do množiny nových koncových stavů vzniklých po determinizaci
            }
            
        } while(!empty($Qnew)); // cyklíme, dokud není pole $Qnew prázdné
        
        // novými množinami vzniklými po determinizacemi nahradíme staré množiny
        self::$setOfStates = $Qd;
        self::$setOfRules = $Rd;
        self::$setOfFiniteStates = $Fd;
        
        self::$FSMIsdeterministic = true; // nastavíme příznak provedení determinizace

    }
    
    /**
     * Metoda pro lexikografické řazení vstupního pole
     * @param $arrayToSort Pole předané odkazem, určené k seřazení
     * @return void
     */
    public static function lexicographicSort(&$arrayToSort)
    {
        usort($arrayToSort, function($a, $b)
        { 
            $a = trim($a, "'"); // v případě symbolu odstraníme apostrofy
            $b = trim($b, "'"); // v případě symbolu odstraníme apostrofy
            return strcmp($a,$b);                        
        }); // anonymní funkce pro lexikografické řazení 
    }
    
    /**
     * Metoda vypíše (D)KA v normální formě
     * @param $outputFile Soubor, kam zapíšeme normální formu KA
     * @return true, nebo false v závislosti na tom, jestli došlo k chybě
     */
    public static function printNormalForm($outputFile)
    {
        $strCointainsNormalForm = "";
        $strCointainsNormalForm .= "(" . PHP_EOL;
        
        // vypíšeme množinu stavů
        $strCointainsNormalForm .= "{";
        self::lexicographicSort(self::$setOfStates);
        $firtsState = true;
        foreach(self::$setOfStates as $state)
        {
            if ($firtsState)
            {
                $strCointainsNormalForm .= $state;
                $firtsState = false;
            }
            else
            {
                $strCointainsNormalForm .= ", " . $state;
            }
        }
        $strCointainsNormalForm .= "}," . PHP_EOL;
        
        // vypíšeme vstupní abecedu
        $strCointainsNormalForm .= "{";
        self::lexicographicSort(self::$inputAlphabet);
        $firtsSymbol = true;
        foreach(self::$inputAlphabet as $symbol)
        {
            if ($firtsSymbol)
            {
                $strCointainsNormalForm .= $symbol;
                $firtsSymbol = false;
            }
            else
            {
                $strCointainsNormalForm .= ", " . $symbol;
            }
        }
        $strCointainsNormalForm .= "}," . PHP_EOL;
        
        // vypíšeme množinu pravidel
        $strCointainsNormalForm .= "{" . PHP_EOL;
        
        // pokud je množina pravidel neprázdná
        if (count(self::$setOfRules) > 0)
        {
            // seřadíme množinu pravidel nejdřív podle primárného klíče (výchozího stavu)
            uksort(self::$setOfRules, function($a, $b)
            { 
                return strcmp($a, $b);               
            });

            // následně seřadíme množinu pravidel podle sekundárních klíčů (symboly) a případně terciálních klíčů (výsledných stavů)
            foreach(self::$setOfRules as $key => $defaultState)
            {
                usort(self::$setOfRules[$key], function($a, $b)
                { 
                    // zjistíme symboly a výsledné stavy dvou pravidel, které začínají stejným výchozím stavem a porovnáme je
                    $resultantStateA = reset($a);
                    $symbolA = key($a);

                    $resultantStateB = reset($b);
                    $symbolB = key($b);

                    $symbolA = trim($symbolA, "'"); // v případě symbolu odstraníme apostrofy
                    $symbolB = trim($symbolB, "'"); // v případě symbolu odstraníme apostrofy

                    $tmpStrCmp = strcmp($symbolA,$symbolB);
                    // pokud jsou sekundární klíče (symboly) stejné, budeme řadit podle terciálních klíčů (výsledných stavů)
                    if ($tmpStrCmp == 0)
                    {
                        return strcmp($resultantStateA, $resultantStateB);
                    }
                    return $tmpStrCmp; 

                }); // anonymní funkce pro lexikografické řazení 
            }

            // nyní vypíšeme lexikograficky seřazená pravidla
            foreach(self::$setOfRules as $defaultState => $symbolsAndResultantStates)
            {
                foreach($symbolsAndResultantStates as $symbolAndResultantState)
                {
                    foreach($symbolAndResultantState as $symbol => $resultantState)
                    {
                        $strCointainsNormalForm .= $defaultState . " " . $symbol .  " -> " . $resultantState . "," . PHP_EOL;
                    }
                }
            }
            $strCointainsNormalForm = substr($strCointainsNormalForm, 0, -2); // odstraníme poslední čárku a konec řádku
            $strCointainsNormalForm .= PHP_EOL; // přidáme zpět konec řádku
        }
        
        $strCointainsNormalForm .= "}," . PHP_EOL;
        
        // vypíšeme poč. stav
        $strCointainsNormalForm .= self::$initialState . "," . PHP_EOL;
        
        // vypíšeme množinu koncových stavů
        $strCointainsNormalForm .= "{";
        self::lexicographicSort(self::$setOfFiniteStates);
        $firstFiniteState = true;
        foreach(self::$setOfFiniteStates as $finiteState)
        {
            if ($firstFiniteState)
            {
                $strCointainsNormalForm .= $finiteState;
                $firstFiniteState = false;
            }
            else
            {
                $strCointainsNormalForm .= ", " . $finiteState;
            }
        }
        $strCointainsNormalForm .= "}" . PHP_EOL;
        
        $strCointainsNormalForm .= ")";
        
        // zapíšeme normální formu na výstup
        if (file_put_contents($outputFile, $strCointainsNormalForm) === false)
        {
            return false;
        }
        
        return true;

    }
    
    /**
     * Metoda analyzuje řetězec a vyhodnocuje, jestli jej KA přijme
     * @param $stringToAnalyze Řetezec, který bude analyzován
     * @return true nebo false, v závislosti na tom, jestli nastala chyba
     */
    public static function stringAnalyze($stringToAnalyze)
    {        
        self::makeTheDeterminization();      // nejdříve provedeme determinizaci KA
        $currentState = self::$initialState; // nastavíme stav na počáteční
        // řetezec převedeme na pole jednotlivých znaků (v utf8)
        $arrayOfCharsOfString = preg_split('//u', $stringToAnalyze, NULL, PREG_SPLIT_NO_EMPTY);
        
        // přes jednotlivé znaky, které představují symboly iterujeme
        foreach($arrayOfCharsOfString as $char)
        {
            $char = "'" . $char . "'"; // doplníme apostrofy, abychom mohli porovnávat se vstupní abecedou
            // pokud se jedná o symbol, který se nenachází ve vstupní abecedě, nastane chyba
            if (!isset(self::$inputAlphabet[$char]))
            {
                return false;
            }
            // pokud pro současný stav není možné provést žádný přechod, vytiskneme 1 a skončíme
            if (!array_key_exists($currentState, self::$setOfRules))
            {
                echo STRING_HAS_NOT_BEEN_ACCEPTED;
                return true;
            }
            $transitionIsPossible = false; // dokud nenalezneme odpovídající přechod, tak bude platit, že není možné provést přechod
            // pokusíme se provést přechod do jiného stavu čtením vstupního symbolu v současného stavu
            foreach (self::$setOfRules[$currentState] as $symbolAndResultantState)
            {
                foreach ($symbolAndResultantState as $symbol => $resultantState)
                {
                    // pokud jsem pro současný stav a symbol nalezli přechod, nastavíme současný stav na cílový stav
                    if ($char == $symbol)
                    {
                        $currentState = $resultantState;
                        $transitionIsPossible = true;
                    }
                }
            }
            
            // pokud jsme nenalezli přechod, tak řetězec nemůže být přijat KA
            if (!$transitionIsPossible) 
            {
                echo STRING_HAS_NOT_BEEN_ACCEPTED;
                return true;
            }
        }
        
        // pokud KA přijímá zadaný řetězec (přečetli jsme jej celý a jsme ve stavu, který je koncový)
        if (isset(self::$setOfFiniteStates[$currentState]))
        {
            echo STRING_HAS_BEEN_ACCEPTED;
        }
        // pokud KA nepříjímá řetězec
        else
        {
            echo STRING_HAS_NOT_BEEN_ACCEPTED;
        }
        
        return true;
    }
    
    /**
     * Metoda přidává pravidlo KA do pole, pokud již existuje, nebude se nové vkládat
     * @param $originalState původní stav, ze kterého je prováděn přechod
     * @param $symbol vstupní symbol 
     * @param $resultantState výsledný stav, do kterého se automat dostane po přechodu
     * @param $arrayLocation Pole, kam budeme pravidlo přidávat
     * @return void
     */
    private static function addRuleToArray($originalState, $symbol, $resultantState, &$arrayLocation)
    {        
        // pokud existuje již stejné pravidlo v množině pravidel, nebudeme toto vkládat
        if (array_key_exists($originalState, $arrayLocation))
        {
            foreach($arrayLocation[$originalState] as $symbolAndResultantState)
            {
                if (array_key_exists($symbol, $symbolAndResultantState))
                {
                    if ($symbolAndResultantState[$symbol] == $resultantState)
                    {
                        return;
                    }
                }
            }
        }
        
        $arrayLocation[$originalState][] = array($symbol => $resultantState); // přidáme nové pravidlo
    }
    
    // debugovací funkce, která vypíše strukturu KA
    public static function printFSMStructure()
    {

        var_dump(self::$setOfStates);
        echo "\n\n";
        var_dump(self::$inputAlphabet);
        echo "\n\n";
        var_dump(self::$setOfRules);
        echo "\n\n";
        var_dump(self::$initialState);
        echo "\n\n";
        var_dump(self::$setOfFiniteStates);
        echo "\n\n";
        
    }
}
