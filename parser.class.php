<?php

#DKA:xherec00

/* 
 * Kódování: UTF-8
 * Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
 * Datum vytvoření: 21. 2. 2015
 */

/**
 * Třída reperezentuje syntaktický analyzátor pro analýzu vstupní reprezentaci KA
 * Pravidla LL gramatiky jsou uvedena v dokumentaci
 */
class Parser {
    
    /**
     * Metoda začne provádět syntaktickou analýzu vstupní reprezentace KA metodou rekurzovního sestupu
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    public static function BeginSyntacticAnalysis()
    {
        return self::nonTerminal_FSM();
    }
    
    /**
     * Metoda simuluje analýzu neterminálu <FSM>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_FSM()
    {
        // pravidlo: <FSM> -> ( { <Q> } , { <SIGMA> } , { <R> } , state, { <F> } ) $ 
        
        // ČÁST: (
        if (Scanner::getNextToken($tokenVal) != TOKEN_LEFT_PARENTHESIS)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
        // ČÁST: (
        // ---------------------------------------------------------------------------------------------
        // ČÁST: { <Q> } ,
        if (Scanner::getNextToken($tokenVal) != TOKEN_LEFT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (($errorCode = self::nonTerminal_Q()) != ALL_OK)
        {
            return $errorCode;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_RIGHT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_COMMA)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        // ČÁST: { <Q> } ,
        // ---------------------------------------------------------------------------------------------
        // ČÁST: { <SIGMA> } ,
        if (Scanner::getNextToken($tokenVal) != TOKEN_LEFT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (($errorCode = self::nonTerminal_SIGMA()) != ALL_OK)
        {
            return $errorCode;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_RIGHT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_COMMA)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        // ČÁST: { <SIGMA> } ,
        // ---------------------------------------------------------------------------------------------        
        // ČÁST: { <R> } ,
        if (Scanner::getNextToken($tokenVal) != TOKEN_LEFT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (($errorCode = self::nonTerminal_R()) != ALL_OK)
        {
            return $errorCode;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_RIGHT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_COMMA)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        // ČÁST: { <R> } ,
        // ---------------------------------------------------------------------------------------------
        // ČÁST: state,
        if (Scanner::getNextToken($tokenVal) != TOKEN_FSM_STATE)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }

        // pokud poč. stav neleží v množině stavů, vrátíme sémantickou chybu
        $tmpStates = FSM::getStates();  
        if (!isset($tmpStates[$tokenVal]))
        {
            return ERROR_SEMANTIC;
        }

        FSM::setInitialState($tokenVal); // nastavíme poč. stav KA
        
        if (Scanner::getNextToken($tokenVal) != TOKEN_COMMA)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        // ČÁST: state,
        // ---------------------------------------------------------------------------------------------
        // ČÁST: { <F> } ,
        if (Scanner::getNextToken($tokenVal) != TOKEN_LEFT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        if (($errorCode = self::nonTerminal_F()) != ALL_OK)
        {
            return $errorCode;   
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_RIGHT_CURLY_BRACKET)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;   
        }
        // ČÁST: { <F> } ,
        // ---------------------------------------------------------------------------------------------
        // ČÁST: ) $
        if (Scanner::getNextToken($tokenVal) != TOKEN_RIGHT_PARENTHESIS)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
        if (Scanner::getNextToken($tokenVal) != TOKEN_EOF)
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
        // ČÁST:  ) $
        // ---------------------------------------------------------------------------------------------
        
        return ALL_OK; // pokud proběhla celá syntaktická analýza daného neterminálu v pořádku, vracíme ALL_OK
    }
    
    /**
     * Metoda simuluje analýzu neterminálu <Q>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_Q()
    {
        // pravidlo: <Q> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <Q> -> state <Q_NEXT>
        else if ($tokenType == TOKEN_FSM_STATE)
        {
            FSM::addState($tokenVal); // přidáme stav do množiny stavů KA
            return self::nonTerminal_Q_NEXT();
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
        /**
     * Metoda simuluje analýzu neterminálu <Q_NEXT>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_Q_NEXT()
    {
        // pravidlo: <Q_NEXT> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <Q_NEXT> -> , state <Q_NEXT>
        else if ($tokenType == TOKEN_COMMA)
        {
            if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_STATE)
            {
                FSM::addState($tokenVal); // přidáme stav do množiny stavů KA
                return self::nonTerminal_Q_NEXT();
            }
            // chyba
            else
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
    /**
     * Metoda simuluje analýzu neterminálu <SIGMA>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_SIGMA()
    {
        // pravidlo: <SIGMA> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // když je prázdná vstupní abeceda, nastane sémantická chyba
            return ERROR_SEMANTIC;
        }
        // pravidlo: <SIGMA> -> input <Q_NEXT>
        else if ($tokenType == TOKEN_FSM_INPUT)
        {
            // vstupním symbolem nemůže být prázdný řetězec
            if ($tokenVal == EPSILON)
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
            
            FSM::addInputSymbol($tokenVal); // přidáme symbol do množiny vstupních symbolů KA
            return self::nonTerminal_SIGMA_NEXT();
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
        /**
     * Metoda simuluje analýzu neterminálu <SIGMA_NEXT>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_SIGMA_NEXT()
    {
        // pravidlo: <SIGMA_NEXT> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <SIGMA_NEXT> -> , input <Q_NEXT>
        else if ($tokenType == TOKEN_COMMA)
        {
            if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_INPUT)
            {
                // vstupním symbolem nemůže být prázdný řetězec
                if ($tokenVal == EPSILON)
                {
                    return ERROR_LEXICAL_OR_SYNTACTIC;
                }
                
                FSM::addInputSymbol($tokenVal); // přidáme symbol do množiny vstupních symbolů KA
                return self::nonTerminal_SIGMA_NEXT();
            }
            // chyba
            else
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
    
    /**
     * Metoda simuluje analýzu neterminálu <R>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_R()
    {
        // pravidlo: <R> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <R> -> state input -> state <R_NEXT>
        else if ($tokenType == TOKEN_FSM_STATE)
        {
            $loadedState = $tokenVal; // poznamenáme si načtený stav
            
            if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_INPUT)
            {
                $loadedSymbol = $tokenVal; // poznamenáme si načtený symbol
                
                if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_TRANSITION)
                {
                    if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_STATE)
                    {
                        // pokud pravidlo obsahuje nededefinované elementy, nastane sémantická chyba
                        if (self::checkIfRuleContainsKnownElements($loadedState, $loadedSymbol, $tokenVal) == false)
                        {
                            return ERROR_SEMANTIC;
                        }
                        
                        FSM::addRule($loadedState, $loadedSymbol, $tokenVal); // přidáme pravidlo KA
                        return self::nonTerminal_R_NEXT();
                    }
                    // chyba
                    else
                    {
                        return ERROR_LEXICAL_OR_SYNTACTIC;
                    }
                }
                // chyba
                else
                {
                    return ERROR_LEXICAL_OR_SYNTACTIC;
                }
            }
            // chyba
            else
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
    /**
     * Metoda simuluje analýzu neterminálu <R_NEXT>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_R_NEXT()
    {
        // pravidlo: <R_NEXT> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <R_NEXT> -> , state input -> state <R_NEXT>
        else if ($tokenType == TOKEN_COMMA)
        {
            if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_STATE)
            {
                $loadedState = $tokenVal; // poznamenáme si načtený stav
                
                if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_INPUT)
                {
                    $loadedSymbol = $tokenVal; // poznamenáme si načtený symbol
                    
                    if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_TRANSITION)
                    {
                        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_STATE)
                        {
                            // pokud pravidlo obsahuje nededefinované elementy, nastane sémantická chyba
                            if (self::checkIfRuleContainsKnownElements($loadedState, $loadedSymbol, $tokenVal) == false)
                            {
                                return ERROR_SEMANTIC;
                            }
                        
                            FSM::addRule($loadedState, $loadedSymbol, $tokenVal); // přidáme pravidlo KA
                            return self::nonTerminal_R_NEXT();
                        }
                        // chyba
                        else
                        {
                            return ERROR_LEXICAL_OR_SYNTACTIC;
                        }
                    }
                    // chyba
                    else
                    {
                        return ERROR_LEXICAL_OR_SYNTACTIC;
                    }
                }
                // chyba
                else
                {
                    return ERROR_LEXICAL_OR_SYNTACTIC;
                }
            }
            // chyba
            else
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
    /**
     * Metoda simuluje analýzu neterminálu <F>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_F()
    {
        // pravidlo: <F> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <F> -> state <Q_NEXT>
        else if ($tokenType == TOKEN_FSM_STATE)
        {
            // pokud koncový stav neleží v množině stavů, vrátíme sémantickou chybu
            $tmpStates = FSM::getStates();  
            if (!isset($tmpStates[$tokenVal]))
            {
                return ERROR_SEMANTIC;
            }
        
            FSM::addFiniteState($tokenVal); // přidáme stav do množiny koncových stavů KA
            return self::nonTerminal_F_NEXT();
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
        /**
     * Metoda simuluje analýzu neterminálu <F_NEXT>
     * @return kód chyby, která se vyskytla při analýze, nebo konstantu ALL_OK pokud analýza proběhne v pořádku
     */
    private static function nonTerminal_F_NEXT()
    {
        // pravidlo: <Q_NEXT> -> epsilon 
        if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_RIGHT_CURLY_BRACKET)
        {
            // vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            Scanner::ungetToken(array(TOKEN_RIGHT_CURLY_BRACKET, $tokenVal));
            return ALL_OK;
        }
        // pravidlo: <Q_NEXT> -> , state <Q_NEXT>
        else if ($tokenType == TOKEN_COMMA)
        {
            if (($tokenType = Scanner::getNextToken($tokenVal)) == TOKEN_FSM_STATE)
            {
                // pokud koncový stav neleží v množině stavů, vrátíme sémantickou chybu
                $tmpStates = FSM::getStates();  
                if (!isset($tmpStates[$tokenVal]))
                {
                    return ERROR_SEMANTIC;
                }
                
                FSM::addFiniteState($tokenVal); // přidáme stav do množiny koncových stavů KA
                return self::nonTerminal_F_NEXT();
            }
            // chyba
            else
            {
                return ERROR_LEXICAL_OR_SYNTACTIC;
            }
        }
        // chyba
        else
        {
            return ERROR_LEXICAL_OR_SYNTACTIC;
        }
    }
    
    /////////////////////  METODY PRO SÉMANTICKÉ KONTROLY /////////////////////
    
    /**
     * Metoda zkontroluje, jestli pravidlo obsahuje prvky, které již byly definovány
     * @param $originalState původní stav, ze kterého je prováděn přechod
     * @param $symbol vstupní symbol 
     * @param $resultantState výsledný stav, do kterého se automat dostane po přechodu
     * @return true nebo false
     */
    public static function checkIfRuleContainsKnownElements($originalState, $symbol, $resultantState)
    {
        $tmpStates = FSM::getStates();

        if (isset($tmpStates[$originalState]))
        {
            $inputAlphabet = FSM::getInputAlphabet();

            if (isset($inputAlphabet[$symbol]) || $symbol == EPSILON)
            {
                if (isset($tmpStates[$resultantState]))
                {
                    return true;
                }
            }
        }

        return false;
    }
    
}